Olá pessoal, Esse repositório pretende ser a base para o repertório de uma banda sem nome ainda, mas que é um projeto antigo de hard rock a ser implementado, assim que encontrar as pessoas certas.

A idéia é manter aqui:

01. Letras
02. Cifras
03. Tablaturas
04. Midis GP6
05. MP4 das músicas 
06. Timbres de pedais

O objetivo desse repositório é organizar num só lugar os itens acima de modo a irmos enriquecendo o repertório 
e mantendo organizado nosso material de estudo e dever de casa.

- O diretório letras é direcionado para o vocalista e backing vocals.

- Cifras é um primeiro passo para nos familiarizarmos com os acordes das músicas.

- Tablaturas dá um direcionamento de baixos, rifs e solos mais simples


- Midis GP6 é uma coleção gigante de peças (observar a pasta _tabs) que abertas no Guitar Pro mostra todo o arranjo com partitura, tablatura e ainda dá pra ouvir a música no formato Midi o que dá um bom backtrack e uma boa noção de ritmos e tempo.

- O diretório MP4 ainda não foi usado, mas espero a medida que as músicas de trabalho forem sendo escolhidas possamos ir adicionando aí os MP4 pra todos se familiarizarem com a versão que será tocada.

- Timbres de pedais é uma coleção de timbres mais próximos dos clássicos orienta na regulagem de efeitos, no diretório App tem os softwares necessários para abrir esses arquivos.


Bom é isso, no menu ao lado vocês tem a opção clonar para baixar para seus computadores, recomendo instalar o App sourcetree para que na medida que forem adicionando coisas no repositório local, possam ir sincronizando o servidor para todos.

É só uma sugestão de método de trabalho e estudos mas acho válido para organizarmos o repertório e que compartilho com vocês, espero que curtam a idéia, as músicas que estão em letras são só exemplos, vamos discutir e selecionar ainda.

;)