Van Halen 1.syx

Created by Jonathan Straub (Lazertronic)

This patch is made to mimic the sound of the VH1 Album.
It uses a Marshall 1959 (1+2) Preamp with medium gain.
I have added a Tape Echo Delay to sound like Ain't Talkin' Bout Love.

YOU WILL NEED EXT CTL PEDALS TO GET THE FULL SOUND!

The onboard CTL pedal controls the Solo Function.
EXT CTL 1 is momentary and will control the Phaser in FX-2.
EXT CTL 2 is momentary and will control the Flanger in FX-1.
The EXP Pedal is controling volume, and will act as a wah pedal
when the EXP PDL SW is activated!

It's basically a VH Patch Tweaker's Dream!

Have fun!

P.S. At the time of this file creation, no editor has been made for the GT-8,
so you will have to use GT-SYX to dump the patch to your GT-8!