Jim's(Sorbz62) Patches:

1.  Ballerina:   Steve Vai harmonised solo song.  Slightly altered from another user.
2.  Bell Tolls 1:  Metallica Rhythm 1
3.  Bell Tolls 2:  Metallica Rhythm 2
4.  Boulevard:     Green Day.  Use control pedal/ patch no pedal to change sounds from distorted with slicer to Clean acoustic to mesa distorted rhythm
5.  Bryan Adams:   Crunchy sound as per Summer of 69.
6.  Chillies Stereo:  For use on Californication. Clean, chorusy. Ctl pedal for boosted solo.
7.  Cliffs of Dover:  Eric Johnson classic.
8.  Crazylittle thing called love:  Acoustic - ctl pedal for Solo
9.  Creed:  Weathered album sound
10. Curtain Swell:  Someone else's patch, altered slightly - awesome orchestra type sound.
11. Darkness 1:  Rock sound with ctl pedal solo
12. Darkness 2:  Rock sound with ctl pedal solo (one of these is Justin's one is Dan's)
13. Dropped C:  Metal sound with pitch shift down to D - very heavy
14. Eric Johnson 1/ Eric Johnson:  2 similar EJ patches
15. EVH:  VH1 type sound
16. Gary Moore direct:  Cold Hearted Era Moore - good for Deep Purple stuff too!
17. Gilmour Time:  awesome for the solo on DSOTM Time track
18. Hendrix solo:  Use with single coil neck p/u. Has wah for lead sound on Watchtower
19. Hendrix Fire:  Marshall with phaser etc - use ctl pedal for boosted lead
20. jim dry metal marshall1:  Dry JCM sound for recording and adding fx later
21. LIZZY 1/ LIZZY2/ LIZZY LEAD:  3 patches for Jailbreak era Lizzy sound
22. Metal Rect:  Chorusy metal sound
23. Metal Rules: Another metal sound
24. Metallica:  Nothing Else Matters sound. Ctl pedal changes from clean w/chrus to the lead mesa distorted sound
25. Morello2:  Whammy for Stone in Love solo
26. Morello clean:  Generic clean Marshall sound
27. Neal Schon 1/ 2/ 3/ 4:  variations on Stone in Love Journey sound ie 80s  LA guitar sound
28. Nickelback:  Awesoem patch - sounds just like How you Remind me song. Ctl Pedal for clean/ chorus to mesa style raunch
29. Nu-metal:  Papa Roach type sound - heavy
30. Gilmour 73:  early PF sound
31. Queen Heavy: Brian May's Marshall era - uses Metal Zone fx
32. Rectifier1:  Used as dry mesa type recording (see marshall one above)
33. Robin Trower:  Marshall mayhem with Uni-V and wah. Ctl pedal for solo
34. Rosalie:   Very middly Marshall sound. Like a wah at half-cock?
35. SRV1:  Texas Flood clean sound - use ctl pedal for solo boost
36. Stereo80s:  Uses Dual L/R stereo for classic LA 80s sound
37. Stevevai: 
38. Still got the Blues
39. Still got the Blues2
40. Still got Blues
41. Survivor:  Another 80s chorusy sound
42. Synth1:  Quite a strong guitar synth - strike hard for good attack
43. u2:  Mega echo clean sound as in the song Pride
44. VH good dry:  Nice dry VH sound for adding fx to later
45. VH Stereo reverb:  Later VH sound - very loud and stereo!
46. Windcriesmary: marshall clean with uni-v - nice warm sound
47. zztop:  overdriven Fender amp for Tush. Ctl Pedal for solo boost

There is also a folder which has all my presets for my gig I just did - with the presets named properly on the GT8, ie with the name of the song. I then load the 8 with the presets in song order!!!

Some of these are adaptations of other guy's patches but I have altered them in some way so I haven't just pinched them!!!!

Thanks,
Jim